import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import moduleEx from './moduleEx'

export default new Vuex.Store({
    modules: {
        moduleEx
    }
})